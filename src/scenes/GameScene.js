import 'phaser';
import {
    defineGrid,
    extendHex
} from 'honeycomb-grid';

import {
    CST
} from "../config/CST";

export default class gameScene extends Phaser.Scene {
    constructor() {
        super('Game');

    }

    preload() {
        this.load.spritesheet('tilemap', 'assets/tilemap.png', {
            frameWidth: 120,
            frameHeight: 104
        });

    }

    create(data) {

        this.game.resize();
        window.mainscene = this;

        this.level = data.level;
        this.startPoint = {
            x: 0,
            y: 0
        };

        this.palleteStartPoint = {
            x: 530,
            y: 650
        };

        // Define Hex and Grid staff
        this.Hex = extendHex({
            size: 60,
            orientation: 'flat'
        });

        this.Grid = defineGrid(this.Hex);
        this.grid = this.Grid.hexagon({
            radius: 3,
            center: [3, 3],
        });
        window.grid = this.grid;
        // Draw grid
        this.grid.forEach(hex => this.drawHex(hex, this.startPoint, CST.colors.green));

        this.drawPallete();
        this.loadLevel();
        this.createPreview(4.5);

        this.input.on("pointermove", this.mouseHandler, this);
        this.input.on('gameobjectdown', this.obecjetClickHandler, this);
        this.input.on('gameobjectover', this.objectOverHander, this);
        this.input.on('gameobjectout', this.objectOutHandler, this);

        this.currentColor = CST.colors.green;



    }


    update() {

    }

    drawHex(hex, startPoint, color) {
        let placeSpritePoint = hex.toPoint().add(startPoint);
        hex.sprite = this.add.sprite(placeSpritePoint.x, placeSpritePoint.y, 'tilemap', color);
        hex.sprite.setOrigin(0);
    }

    mouseHandler(e) {
        if (e.isDown) {
            let hexCoordinates = this.Grid.pointToHex(e.position.x - this.startPoint.x, e.position.y - this.startPoint.y)

            let hex = this.grid.get(hexCoordinates);
            if (hex) {
                hex.sprite.setFrame(this.currentColor);
            }
        }
    }

    obecjetClickHandler(e, object) {
        if (object.name) {
            console.log(object.name);
            this.currentColor = object.color;
            this.currentHex.sprite.setFrame(this.currentColor);
        }
    }

    objectOverHander(e, object) {
        if (object.name) {
            object.depth = 9;
            object.setScale(1.1);
            object.setRotation(-0.2);
        }
    }

    objectOutHandler(e, object) {
        if (object.name) {
            object.depth = 2;
            object.setScale(1);
            object.setRotation(0);
        }
    }

    saveMap() {
        let map = [];

        this.grid.forEach(hex => map.push({
            x: hex.x,
            y: hex.y,
            color: hex.sprite.frame.name
        }));
        return JSON.stringify(map);

    }

    loadLevel() {
        this.level.forEach(tile => this.grid.get([tile.x, tile.y]).sprite.setFrame(tile.color));
    }

    createPreview(scale) {
        let sHex = extendHex({
            size: 60 / scale,
            orientation: 'flat'
        });
        let sGrid = defineGrid(sHex);
        let sgrid = sGrid.hexagon({
            radius: 3,
            center: [3, 3],
        });
        this.level.forEach(tile => {
            let hex = sgrid.get([tile.x, tile.y]);
            let placeSpritePoint = hex.toPoint().add({
                x: 0,
                y: 0
            });
            hex.sprite = this.add.sprite(placeSpritePoint.x, placeSpritePoint.y, 'tilemap', tile.color);
            hex.sprite.setScale(1 / scale);
            hex.sprite.setOrigin(0);

        })

    }

    drawPallete() {

        this.greenHex = this.Hex({
            x: 0,
            y: 0
        });
        this.drawHex(this.greenHex, this.palleteStartPoint, CST.colors.green);
        let points = this.greenHex.corners();
        let shape = new Phaser.Geom.Polygon(points);
        this.greenHex.sprite.setInteractive(shape, Phaser.Geom.Polygon.Contains);
        this.greenHex.sprite.name = "Green";
        this.greenHex.sprite.color = CST.colors.green;

        this.blueHex = this.Hex({
            x: 1,
            y: 0
        });
        this.drawHex(this.blueHex, this.palleteStartPoint, CST.colors.blue);
        this.blueHex.sprite.setInteractive(shape, Phaser.Geom.Polygon.Contains);
        this.blueHex.sprite.name = 'Blue';
        this.blueHex.sprite.color = CST.colors.blue;

        this.greyHex = this.Hex({
            x: 1,
            y: -1
        });
        this.drawHex(this.greyHex, this.palleteStartPoint, CST.colors.grey);
        this.greyHex.sprite.setInteractive(shape, Phaser.Geom.Polygon.Contains);
        this.greyHex.sprite.name = "Grey";
        this.greyHex.sprite.color = CST.colors.grey;

        this.brownHex = this.Hex({
            x: 2,
            y: 0
        });
        this.drawHex(this.brownHex, this.palleteStartPoint, CST.colors.brown);
        this.brownHex.sprite.setInteractive(shape, Phaser.Geom.Polygon.Contains);
        this.brownHex.sprite.name = "Brown";
        this.brownHex.sprite.color = CST.colors.brown;

        this.currentHex = this.Hex({
            x: 0,
            y: 0
        });
        this.drawHex(this.currentHex, {
            x: 500,
            y: 10
        }, CST.colors.green);
        this.currentHex.sprite.color = CST.colors.green;
        this.currentHex.sprite.setInteractive();
        this.currentHex.sprite.on('pointerdown', this.saveHandler);
        this.currentColor = CST.colors.green;
    }

    saveHandler(e) {
        console.log(this.scene.saveMap());
    }

}