import 'phaser';
import config from './config/config';
import gameScene from './scenes/GameScene';

class Game extends Phaser.Game {
  constructor() {
    super(config);
    this.scene.add('Game', gameScene);
    this.level = [{
      x: 0,
      y: 2,
      color: 3
    }, {
      x: 0,
      y: 3,
      color: 1
    }, {
      x: 0,
      y: 4,
      color: 3
    }, {
      x: 0,
      y: 5,
      color: 1
    }, {
      x: 1,
      y: 1,
      color: 1
    }, {
      x: 1,
      y: 2,
      color: 3
    }, {
      x: 1,
      y: 3,
      color: 3
    }, {
      x: 1,
      y: 4,
      color: 1
    }, {
      x: 1,
      y: 5,
      color: 3
    }, {
      x: 2,
      y: 1,
      color: 3
    }, {
      x: 2,
      y: 2,
      color: 1
    }, {
      x: 2,
      y: 3,
      color: 3
    }, {
      x: 2,
      y: 4,
      color: 3
    }, {
      x: 2,
      y: 5,
      color: 1
    }, {
      x: 2,
      y: 6,
      color: 3
    }, {
      x: 3,
      y: 0,
      color: 1
    }, {
      x: 3,
      y: 1,
      color: 3
    }, {
      x: 3,
      y: 2,
      color: 3
    }, {
      x: 3,
      y: 3,
      color: 1
    }, {
      x: 3,
      y: 4,
      color: 3
    }, {
      x: 3,
      y: 5,
      color: 3
    }, {
      x: 3,
      y: 6,
      color: 3
    }, {
      x: 4,
      y: 1,
      color: 3
    }, {
      x: 4,
      y: 2,
      color: 3
    }, {
      x: 4,
      y: 3,
      color: 1
    }, {
      x: 4,
      y: 4,
      color: 3
    }, {
      x: 4,
      y: 5,
      color: 1
    }, {
      x: 4,
      y: 6,
      color: 3
    }, {
      x: 5,
      y: 1,
      color: 3
    }, {
      x: 5,
      y: 2,
      color: 1
    }, {
      x: 5,
      y: 3,
      color: 3
    }, {
      x: 5,
      y: 4,
      color: 1
    }, {
      x: 5,
      y: 5,
      color: 1
    }, {
      x: 6,
      y: 2,
      color: 1
    }, {
      x: 6,
      y: 3,
      color: 1
    }, {
      x: 6,
      y: 4,
      color: 1
    }, {
      x: 6,
      y: 5,
      color: 1
    }];

    this.scene.start('Game', {
      level: this.level
    });
  }

  resize() {
    let canvas = document.querySelector("canvas");
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    if (windowHeight >= windowWidth) {
      canvas.style.width = windowWidth + "px";
      canvas.style.height = windowWidth + "px";
    } else {
      canvas.style.width = windowHeight + "px";
      canvas.style.height = windowHeight + "px";

    }
  }
}

let game = new Game();
window.addEventListener("resize", game.resize, false);
window.game = game;