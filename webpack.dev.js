'use strict';

const webpack = require('webpack');
const path = require('path');
const baseConfig = require('./webpack.common.js');
const merge = require('webpack-merge');

module.exports = merge(baseConfig, {
    output: {
        path: path.resolve(__dirname, 'build'),
        publicPath: '/build/',
        filename: 'project.bundle.js'
    },
    devtool: 'source-map',
    mode: 'development',
    devServer: {
        inline: true,
        port: '8000',
    },

});