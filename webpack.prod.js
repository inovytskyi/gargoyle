'use strict';

const webpack = require('webpack');
const path = require('path');
const baseConfig = require('./webpack.common.js');
const merge = require('webpack-merge');

module.exports = merge(baseConfig, {
    mode: "production",
    output: {
        path: path.resolve(__dirname, 'prodbuild'),
        publicPath: '/build/',
        filename: 'project.bundle.js'
    },
});